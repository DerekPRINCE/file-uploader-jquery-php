<?php
@require_once( '../utils/utils.php' );
$dates = new Dates();
if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    if ( isset( $_FILES['files'] ) ) {
        if ( isset( $_POST['extensions'] ) ) {
            $extensions = $_POST['extensions'];
        } else {
            $extensions = ['jpg', 'jpeg', 'png', 'gif'];
        }
        if ( isset( $_POST['p_fig'] ) ) {
            $paths = get_paths_from_pfig( $_POST['p_fig'] );
        } else {
            $paths = array(
                'physicalPath'=>'../uploads/',
                'absolutePath'=>'../uploads/'
            );
        }
        $errors = [];
        $all_files = count( $_FILES['files']['tmp_name'] );
        $urls = array();

        for ( $i = 0; $i < $all_files; $i++ ) {
            $file_name = $_FILES['files']['name'][$i];
            $file_tmp = $_FILES['files']['tmp_name'][$i];
            $file_type = $_FILES['files']['type'][$i];
            $file_size = $_FILES['files']['size'][$i];
            $file_ext = strtolower( end( explode( '.', $_FILES['files']['name'][$i] ) ) );

            $final_file_name = sha1( $dates->timeStamp().$file_name ).$file_ext;
            $file = $paths['physicalPath'] . $final_file_name ;

            if ( !in_array( $file_ext, $extensions ) ) {
                $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
            }

            if ( $file_size > 2097152 ) {
                $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
            }

            if ( empty( $errors ) ) {
                move_uploaded_file( $file_tmp, $file );
                array_push( $urls, array(
                    'physicalPath'=>$paths['physicalPath'] . $final_file_name,
                    'absolutePath'=>$paths['absolutePath'] . $final_file_name
                ) );
            }
        }

        if ( $errors ) {
            echo json_encode( array(
                'success'=>false,
                'errors'=>$errors,
                'status_code'=>0,
                'status_message'=>'Failed.',
                'message'=>'An error occured.',
                'data'=>array(
                    'urls'=>null
                )
            ) );
        } else {
            echo json_encode( array(
                'success'=>true,
                'errors'=>null,
                'status_code'=>1,
                'status_message'=>'Successful.',
                'message'=>'All files were uploaded.',
                'data'=>array(
                    'urls'=>$urls
                )
            ) );
        }
    }
}

function get_paths_from_pfig( $p_fig ) {
    return json_decode( @file_get_contents( '../config/files.config.json' )[$p_fig], true );
}
